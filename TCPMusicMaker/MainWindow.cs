﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TCPMusicMaker
{
    public partial class MainWindow : Form
    {
        // ########## Attributes ##########
        private readonly string defaultFilePath;    // desktop folder path
        private string currentFilePath;
        
        
        private Music music;
        
        // ########## Methods ##########
        public MainWindow()     // Initialize variables.
        {
            InitializeComponent();

            defaultFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            currentFilePath = "";
        }

        private void openFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult fileOK;

            openFileDialog.FileName = "";
            openFileDialog.Title = "Select text file for input";
            openFileDialog.Filter = "Text files|*.txt";
            openFileDialog.InitialDirectory = defineFilePath();

            fileOK = openFileDialog.ShowDialog();

            if (fileOK == DialogResult.Cancel)
                return;

            currentFilePath = openFileDialog.FileName;

            OpenFile();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult confirm;
            string confirmationMessage = "Do you REALLY want to exit?\r\nAnything not saved will be sent to Niflheim.";

            confirm = MessageBox.Show(confirmationMessage, "OH, REALLY?", MessageBoxButtons.YesNo, MessageBoxIcon.Information);

            if (confirm == DialogResult.Yes)
                Application.Exit();
        }

        private void saveFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = DialogResult.OK;
            if (currentFilePath == "")
                result = saveFileAs();

            if (result == DialogResult.Cancel)
                return;

            if (File.Exists(currentFilePath))
                File.Delete(currentFilePath);

            StreamWriter file = File.CreateText(currentFilePath);
            file.Write(inputBox.Text);
            file.Close();
        }

        private void saveFileAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileAs();
        }

        private DialogResult saveFileAs()
        {
            saveFileDialog.InitialDirectory = defineFilePath();
            saveFileDialog.Title = "Save text file";
            saveFileDialog.Filter = "Text file|*.txt";
            saveFileDialog.FileName = "";

            DialogResult confirm = saveFileDialog.ShowDialog();

            if (confirm == DialogResult.Cancel)
                return confirm;

            currentFilePath = saveFileDialog.FileName;

            StreamWriter file = File.CreateText(currentFilePath);
            file.Write(inputBox.Text);
            file.Close();
            return confirm;
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            // Read text input, create list with frequency and duration to be played.
            if (music != null)
                music.Stop(); //May be better than doing nothing!

            music = new Music();
            music.GenerateFromString(inputBox.Text);
        }

        private void buttonPlayPause_Click(object sender, EventArgs e)
        {
            if (music == null)
                return;

            music.SetVolume(volumeTrackBar.Value);
            music.PlayPause();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (music == null)
                return;

            music.Stop();

            music = null;
        }

        private void volumeTrackBar_ValueChanged(object sender, EventArgs e)
        {
            if (music == null)
                return;

            music.SetVolume(volumeTrackBar.Value);
        }

        // ##### Helpers #####
        /// <summary>
        /// Returns the directory of the last file opened. If no file was opened, returns Desktop directory.
        /// </summary>
        /// <returns>Last file directory or Desktop directory.</returns>
        private string defineFilePath()
        {
            if (currentFilePath == "")      // If a file has been opened before, the directory will be where this file is.
                return defaultFilePath;

            char[] filePath = currentFilePath.ToCharArray();
            int index = filePath.Length;
            index--;            // index points to the last valid position

            while (filePath[index] != '\\' && index > 0)        // finds the last "\" in the path string
                index--;

            if (index == 0)
                return defaultFilePath;     // checks for consistency

            string finalPath = "";

            for (int i = 0; i <= index; i++)        // copies "valid" path string (does this directory really exists?)
                finalPath += filePath[i];

            return finalPath;
        }

        private void OpenFile()
        {
            if (!File.Exists(currentFilePath))
                return;

            StreamReader inputFile = File.OpenText(currentFilePath);
            
            inputBox.Text = inputFile.ReadToEnd();
            
            inputFile.Close();
        }
    }
}
