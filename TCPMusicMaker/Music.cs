﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace TCPMusicMaker
{
    class Music
    {
        List<MusicalNote> musicNotes;
        List<TempoChange> tempoList;
        private WaveSound musicSound;
        private int compassSize;
        //private int currentTempo;
        private int compassCounter = 0;
        private Timer tempoController;

        public const int defaultOctave = 4;
        public const int defaultTempo = 1000;

        private bool firstPlay;
        private bool isPlaying;

        public struct TempoChange
        {
            public int index;
            public int newBPM;
        }
        
        public Music()
        {
            this.musicNotes = new List<MusicalNote>();
            this.tempoList = new List<TempoChange>();
            this.compassSize = defaultTempo; //1s
            this.firstPlay = true;
            this.isPlaying = false;
        }

        public Music(int compassSize)
        {
            this.musicNotes = new List<MusicalNote>();
            this.tempoList = new List<TempoChange>();
            this.compassSize = compassSize;
            this.firstPlay = true;
            this.isPlaying = false;
        }

        private void PlayNextCompass(Object a)
        {
            if (!isPlaying)
                return;
            
            if (compassCounter >= musicNotes.Count)
            {
                if (musicSound != null)
                {
                    musicSound.Stop();
                    musicSound = null;
                    tempoController.Dispose();
                }
                return;
            }
            if (musicSound == null)
                musicSound = new WaveSound(musicNotes[compassCounter++].GetPitch(), 0.25f);
            else
            {
                musicSound.Stop(); //If musicSound is not null, then it means there must be a sound playing
                musicSound.SetFrequency(musicNotes[compassCounter++].GetPitch());
            }
            /*try
            {*/
            musicSound.InitWaveAndPlay();

            foreach (TempoChange t in tempoList)
                if (t.index == compassCounter)
                    SetBPM(t.newBPM);
            /*}
            catch
            {
                Console.WriteLine(compassCounter); //125 is a magic exception number. or 250, when I double the playing speed... this is fuckin strange.
                Console.WriteLine(musicNotes[compassCounter].GetNote());
                Console.WriteLine(musicNotes[compassCounter].GetPitch());
                Console.WriteLine(musicNotes.Count); //In reality, the magic constant is 62.5 seconds - the magic exception number * compassSize
            }*/
        }

        /// <summary>
        /// Adds a note to the music.
        /// </summary>
        /// <param name="note">Note to be added</param>
        /// <param name="duration">Duration of note in compassSize units</param>
        public void AddCompass(MusicalNote note, int duration)
        {
            for (int i = 0; i < duration; i++)
                musicNotes.Add(note);
        }

        public MusicalNote GetNote(int index)
        {
            return musicNotes[index];
        }

        public void GenerateFromString(string input)
        {
            List<MusicalInfo> musicInfo = ParseText(input);
            this.musicNotes = MakeNotesFromInfo(musicInfo);
        }

        private List<MusicalInfo> ParseText(string input)
        {
            List<MusicalInfo> musicInfo = new List<MusicalInfo>();

            foreach (char c in input)
                musicInfo.Add(new MusicalInfo(c));

            return musicInfo;
        }

        private List<MusicalNote> MakeNotesFromInfo(List<MusicalInfo> musicInfo)
        { 
            List<MusicalNote> notes = new List<MusicalNote>();
            int currentOctave = defaultOctave;
            int checkpoint = 0;
            int currentBpm = compassSize;
            MusicalAction action;

            foreach (MusicalInfo info in musicInfo)
            {
                if (info.IsNote())
                    notes.Add(new MusicalNote(info.GetNote(), currentOctave));
                else
                {
                    action = new MusicalAction(info.GetAction(), currentOctave, checkpoint, currentBpm);
                    action.ExecuteAction(notes);
                    currentOctave = action.currentOctave;
                    checkpoint = action.checkpoint;
                    if (action.changedTempo)
                    {
                        tempoList.Add(action.tempoChange);
                        currentBpm = action.tempoChange.newBPM;
                    }
                }
            }

            return notes;
        }

        public void SetVolume(int volume)
        {
            if (musicSound == null)
                return;

            musicSound.SetVolume(volume);
        }

        public void PlayPause()
        {
            if (firstPlay)
            {
                compassCounter = 0;
                tempoController = new Timer(PlayNextCompass, null, 0, compassSize);
                firstPlay = false;
                isPlaying = true;
                return;
            }

            if (musicSound != null)
            {
                musicSound.PlayPause();
                isPlaying = !isPlaying;
            }
        }

        public void SetBPM(int time)
        {
            if (!isPlaying || tempoController == null)
                return;

            int oldCompass = compassSize;
            compassSize = time;
            tempoController.Change(oldCompass, compassSize);
        }

        public void Stop()
        {
            if (musicSound != null)
            {
                musicSound.Stop();
                tempoController.Dispose();
            }
        }
    }
}
