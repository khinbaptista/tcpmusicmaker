﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCPMusicMaker
{
    public enum Action
    {
        Repeat, Sharpen, Flatten, RaiseOctave,
        LowerOctave, Triplicate, DefaultOctave,
        RaiseBPM, LowerBPM, Nothing
    }

    class MusicalAction
    {
        private Action action;
        public int currentOctave;
        public int checkpoint;
        public int bpm;
        public Music.TempoChange tempoChange;
        public bool changedTempo = false;

        private const int bpmIncrement = 200; //Try maybe another value later.

        public MusicalAction(Action action, int currentOctave, int checkpoint, int bpm)
        {
            this.action = action;
            this.currentOctave = currentOctave;
            this.checkpoint = checkpoint;
            this.bpm = bpm;     // <- it was always 0, that's why!
        }

        public void ExecuteAction(List<MusicalNote> notes)
        {
            if (action == Action.Repeat)
                Repeat(notes);
            else if (action == Action.LowerOctave)
                LowerOctave();
            else if (action == Action.RaiseOctave)
                RaiseOctave();
            else if (action == Action.Sharpen)
                Sharpen(notes);
            else if (action == Action.Flatten)
                Flatten(notes);
            else if (action == Action.RaiseBPM)
                RaiseBPM(notes);
            else if (action == Action.LowerBPM)
                LowerBPM(notes);
            else if (action == Action.DefaultOctave)
                DefaultOctave();
            else if (action == Action.Triplicate)
                Triplicate(notes);
            else //Action.Nothing
                CopyPreviousNote(notes);
        }

        private void Repeat(List<MusicalNote> notes)
        {
            //Sets a new checkpoint and copies everything from the previous one to the new one
            List<MusicalNote> repeatedRange = notes.GetRange(checkpoint, notes.Count - checkpoint);
            notes.AddRange(repeatedRange); //that shallowcopy thing might give us problems here!!!
            checkpoint = notes.Count;
        }

        private void RaiseOctave()
        {
            currentOctave++;
        }

        private void LowerOctave()
        {
            if (currentOctave > -1)
                currentOctave--;
        }

        private void Sharpen(List<MusicalNote> notes)
        {
            int lastIndex = notes.Count - 1;
            try
            {
                MusicalNote lastElement = notes.ElementAt(lastIndex);
                MusicalNote sharpened = lastElement.Sharpen();
                notes.RemoveAt(lastIndex);
                notes.Add(sharpened);
            }
            catch
            {
                return;
            }
        }

        private void Flatten(List<MusicalNote> notes)
        {
            int lastIndex = notes.Count - 1;
            try
            {
                MusicalNote lastElement = notes.ElementAt(lastIndex);
                MusicalNote flattened = lastElement.Flatten();
                notes.RemoveAt(lastIndex);
                notes.Add(flattened);
            }
            catch
            {
                return;
            }
        }

        private void RaiseBPM(List<MusicalNote> notes)
        {
            int lastIndex = notes.Count;
            if (lastIndex == 0)
                notes.Add(new MusicalNote(Note.Silence, 0));
            if (bpm - bpmIncrement <= 0)
                return;
            tempoChange.index = lastIndex;
            tempoChange.newBPM = bpm - bpmIncrement; //Subtracting actually raises playing speed
            changedTempo = true;
        }

        private void LowerBPM(List<MusicalNote> notes)
        {
            int lastIndex = notes.Count;
            if (lastIndex == 0)
                notes.Add(new MusicalNote(Note.Silence, 0));
            tempoChange.index = lastIndex;
            tempoChange.newBPM = bpm + bpmIncrement;
            changedTempo = true;
        }

        private void DefaultOctave()
        {
            currentOctave = Music.defaultOctave;
        }

        private void Triplicate(List<MusicalNote> notes)
        {
            int lastIndex = notes.Count - 1;
            MusicalNote lastElement = notes.ElementAt(lastIndex);
            notes.Add(lastElement);
            notes.Add(lastElement);
        }

        private void CopyPreviousNote(List<MusicalNote> notes)
        {
            int lastIndex = notes.Count - 1;
            MusicalNote lastElement = notes.ElementAt(lastIndex);
            notes.Add(lastElement);
        }
    }
}
