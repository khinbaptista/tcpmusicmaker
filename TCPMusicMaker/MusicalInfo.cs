﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCPMusicMaker
{
    class MusicalInfo
    {
        /*
         * A MusicalInfo is either a Note or an Action (to be performed over previous/following notes)
         * */

        private Note note = Note.Silence;
        private Action action = Action.Nothing;
        private bool isNote = false;

        public MusicalInfo(char c)
        {
            Mapping(c);
        }

        public bool IsNote()
        {
            return isNote;
        }

        public bool IsAction()
        {
            return !IsNote();
        }

        public Note GetNote()
        {
            return this.note;
        }

        public Action GetAction()
        {
            return this.action;
        }

        private void Mapping(char c) //Hi, I'm a big and ugly brute force mapping method.
        {
            int asciic = (int)Char.ToUpper(c);

            if (IsNote(asciic))
            {
                this.note = MakeNote(asciic);
                this.isNote = true;
            }
            else if (IsSpace(asciic))
            {
                this.note = Note.Silence;
                this.isNote = true;
            }
            else if (IsExclamation(asciic))
                this.action = Action.Repeat;
            else if (IsVowel(asciic))
                this.action = Action.Sharpen;
            else if (IsConsonant(asciic))
                this.action = Action.Flatten;
            else if (IsEvenDigit(asciic))
                this.action = Action.RaiseOctave;
            else if (IsOddDigit(asciic))
                this.action = Action.LowerOctave;
            else if (IsPoint(asciic))
                this.action = Action.Triplicate;
            else if (IsNewLine(asciic))
                this.action = Action.DefaultOctave;
            else if (IsSemicolon(asciic))
                this.action = Action.LowerBPM;
            else if (IsComma(asciic))
                this.action = Action.RaiseBPM;
            else
                this.action = Action.Nothing;
        }

        private bool IsLetter(int asciic)
        {
            if (asciic >= (int)'A' && asciic <= (int)'Z')
                return true;
            return false;
        }

        private bool IsNote(int asciic)
        {
            if (asciic >= (int)'A' && asciic <= (int)'G')
                return true;
            return false;
        }

        private bool IsSpace(int asciic)
        {
            if (asciic == (int)' ')
                return true;
            return false;
        }

        private bool IsExclamation(int asciic)
        {
            if (asciic == (int)'!')
                return true;
            return false;
        }

        private bool IsVowel(int asciic)
        {
            if (asciic == (int)'E' ||
                asciic == (int)'O' ||
                asciic == (int)'I' ||
                asciic == (int)'U')
                return true;
            return false;
        }

        private bool IsConsonant(int asciic)
        {
            if (IsLetter(asciic) && !IsVowel(asciic) && !IsNote(asciic))
                return true;
            return false;
        }

        private bool IsDigit(int asciic)
        {
            if (asciic >= (int)'0' && asciic <= (int)'9')
                return true;
            return false;
        }

        private bool IsEvenDigit(int asciic)
        {
            if (IsDigit(asciic) && (asciic % 2 == 0))
                return true;
            return false;
        }

        private bool IsOddDigit(int asciic)
        {
            if (IsDigit(asciic) && (asciic % 2 == 1))
                return true;
            return false;
        }

        private bool IsPoint(int asciic)
        {
            if (asciic == (int)'.' || asciic == (int)'?')
                return true;
            return false;
        }

        private bool IsNewLine(int asciic)
        {
            if (asciic == 10 || asciic == 13) //Line feed or carriage return
                return true;
            return false;
        }

        private bool IsSemicolon(int asciic)
        {
            if (asciic == ';')
                return true;
            return false;
        }

        private bool IsComma(int asciic)
        {
            if (asciic == ',')
                return true;
            return false;
        }

        private Note MakeNote(int asciic)
        {
            Note note;

            switch (asciic)
            {
                case 'A': note = Note.A; break;
                case 'B': note = Note.B; break;
                case 'C': note = Note.C; break;
                case 'D': note = Note.D; break;
                case 'E': note = Note.E; break;
                case 'F': note = Note.F; break;
                case 'G': note = Note.G; break;
                default: note = Note.Silence; break;
            }

            return note;
        }
    }
}
