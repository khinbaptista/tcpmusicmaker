﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCPMusicMaker
{
    public enum Note : int { Silence = -1, C = 0, Db, D, Eb, E, F, Gb, G, Ab, A, Bb, B }

    class MusicalNote
    {
        private float pitch;
        private int octave;
        private Note note;

        public MusicalNote(Note note, int octave)
        {
            this.octave = octave;
            this.note = note;
            if (note == Note.Silence)
                this.pitch = 0;
            else
                this.pitch = CalculateNotePitch(GetNoteOrdinal(note));
        }

        public float GetPitch()
        {
            return this.pitch;
        }

        public int GetOctave()
        {
            return this.octave;
        }

        public Note GetNote()
        {
            return this.note;
        }

        private int GetNoteOrdinal(Note note)
        {
            return 12 * (this.octave + 1) + (int)note + 1;
        }

        private float CalculateNotePitch(int ordinal)
        {
            return (float)(440f * (Math.Pow((double)2.0f, (((double)ordinal - 70) / 12))));
        }

        public MusicalNote Sharpen()
        {
            int oldOctave = this.GetOctave();
            Note oldNote = this.GetNote();
            int newOctave = oldOctave + (((int)oldNote + 1) / 12);
            Note newNote = (Note)Remainder((int)oldNote + 1, 12);

            return new MusicalNote(newNote, newOctave);
        }

        public MusicalNote Flatten()
        {
            int oldOctave = this.GetOctave();
            Note oldNote = this.GetNote();
            int newOctave = oldOctave;
            if (oldNote == Note.C)
                newOctave = oldOctave - 1;
            Note newNote = (Note)Remainder((int)oldNote - 1, 12);

            return new MusicalNote(newNote, newOctave);
        }

        private int Remainder(int n, int div) //This function implements positive remainder to be used 
        {
            int cSharpRemainder = n % div;
            if (cSharpRemainder >= 0)
                return cSharpRemainder;
            else
                return cSharpRemainder + div;
        }
    }
}
