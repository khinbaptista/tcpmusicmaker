﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NAudio;
using NAudio.Wave;

namespace TCPMusicMaker
{
    class WaveSound
    {
        private WaveOut waveOut;
        private float frequency;
        private float amplitude;

        private const int maxVolume = 100;
        private const int minVolume = 0;
        private const int maxSampleRate = 44000;
        private const int mono = 1;

        public WaveSound(float frequency, float amplitude)
        {
            this.frequency = frequency;
            this.amplitude = amplitude;

            InitWaveSound();
        }

        public float GetFrequency()
        {
            return this.frequency;
        }

        public void SetFrequency(float f)
        {
            this.frequency = f;
        }

        public void InitWaveSound()
        {
            var sineWaveProvider = new SineWaveProvider32();
            sineWaveProvider.SetWaveFormat(maxSampleRate, mono);
            sineWaveProvider.Frequency = frequency;
            sineWaveProvider.Amplitude = amplitude;
            waveOut = new WaveOut();
            waveOut.Init(sineWaveProvider);
            //waveOut.Play(); //Sometimes there's a Null reference exception here
            //waveOut.Pause();
        }

        public void InitWaveAndPlay()
        {
            InitWaveSound();
            Play();
        }

        public bool IsPlaying()
        {
            if (waveOut.PlaybackState == PlaybackState.Playing)
                return true;
            return false;
        }

        public void Play()
        {
            if (!IsPlaying())
                PlayPause();
        }

        public void PlayPause()
        {
            if (waveOut == null)
                return;

            if (waveOut.PlaybackState == PlaybackState.Paused)
                waveOut.Play();
            else if (waveOut.PlaybackState == PlaybackState.Playing)
                waveOut.Pause();
            else if (waveOut.PlaybackState == PlaybackState.Stopped)
                waveOut.Play(); //Try this...
        }

        public void Stop()
        {
            if (waveOut == null)
                return;

            waveOut.Stop();
        }

        /// <summary>
        /// Sets the volume.
        /// </summary>
        /// <param name="volume">Integer between 0 and 100.</param>
        public void SetVolume(int volume)
        {
            if (VolumeOutOfRange(volume))
                throw new ArgumentOutOfRangeException();

            float vol = ScaleVolume(volume);

            waveOut.Volume = vol;
        }

        private bool VolumeOutOfRange(int volume)
        {
            if (volume > maxVolume || volume < minVolume)
                return true;
            return false;
        }

        private float ScaleVolume(int trackBarValue)
        {
            return (float)trackBarValue / (float)maxVolume;
        }
    }

    public class SineWaveProvider32 : WaveProvider32
    {
        int sample;
        
        public SineWaveProvider32()
        {
            Frequency = 1000;
            Amplitude = 0.25f; // let's not hurt our ears            
        }

        public float Frequency { get; set; }
        public float Amplitude { get; set; }

        public override int Read(float[] buffer, int offset, int sampleCount)
        {
            int sampleRate = WaveFormat.SampleRate;
            for (int n = 0; n < sampleCount; n++)
            {
                buffer[n + offset] = (float)(Amplitude * Math.Sin((2 * Math.PI * sample * Frequency) / sampleRate));
                sample++;
                if (sample >= sampleRate) sample = 0;
            }
            return sampleCount;
        }
    }
}
